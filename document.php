<?php include("test-log.php");?>

<!DOCTYPE html>
<html lang="fr">
<html>
    <?php include("include-head.php");?>
    <body>
    <div class="body-inner">

        <?php include("include-header-profil.php");?>

        <section id="page-content" data-bg-parallax="images/29.jpg" >
            <div class="container">

                <div class="card bg-light">

                    <div class="card-header bg-light">
                        <div class="row">
                            <div class="col-lg-3" >
                                <h2>Document</h2>
                            </div>
                            <div class="col-lg-9" >
                                <ul class="nav nav-tabs nav-fill">
                                    <li class="nav-item"><a class="nav-link" href="profil.php">Profil</a></li>
                                    <li class="nav-item"><a class="nav-link" href="calender.php">Calendrier</a></li>
                                    <li class="nav-item"><a class="nav-link active" href="document.php">Document</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3" ></div>
                            <div class="col-lg-9"> 
                                <button type="button" href="#"
                                class="btn btn-warning btn-shadow float-right" style="color:black">Ajouter un document</button>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                
                        <div class="row">

                            <div class="col-lg-3">
                                <div class="card" style="width: 18rem; height: 200px;">
                                    <div class="card-body">
                                        <h5 class="card-title">Allo</h5>
                                        <p class="card-text">Ce document vous dit allo!</p>
                                    </div>
                                    <div class="card-footer text-muted mx-auto">
                                        <a href="Allo.pdf" class="btn btn-primary">Télécharger</a>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-3">
                                <div class="card" style="width: 18rem; height: 200px;">
                                    <div class="card-body">
                                        <h5 class="card-title">Allo2</h5>
                                        <p class="card-text">Ce document vous dit aussi allo!</p>
                                    </div>
                                    <div class="card-footer text-muted mx-auto">
                                        <a href="Allo2.pdf" class="btn btn-primary">Télécharger</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="card" style="width: 18rem; height: 200px;">
                                    <div class="card-body">
                                        <h5 class="card-title">Allo2</h5>
                                        <p class="card-text">Ce document vous dit aussi allo!</p>
                                    </div>
                                    <div class="card-footer text-muted mx-auto">
                                        <a href="Allo2.pdf" class="btn btn-primary">Télécharger</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="card" style="width: 18rem; height: 200px;">
                                    <div class="card-body">
                                        <h5 class="card-title">Allo2</h5>
                                        <p class="card-text">Ce document vous dit aussi allo!</p>
                                    </div>
                                    <div class="card-footer text-muted mx-auto">
                                        <a href="Allo2.pdf" class="btn btn-primary">Télécharger</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include("include-footer.php");?>
    </div>
    <?php include("include-script.php");?>
    </body>
</html>