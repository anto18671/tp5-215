<?php include("test-log.php");?>

<!DOCTYPE html>
<html lang="fr">
<html>
    <?php include("include-head.php");?>
    <body>
    <div class="body-inner">

    <?php include("include-header-profil.php");?>

        <section id="page-content" data-bg-parallax="images/29.jpg">
            <div class="container">
                <div class="row">
                <div class="content col-lg-9 center">
                    <div class="card">
                        <div class="card-header">
                            <div class="content col-lg-9 center" style="text-align: center;">
                                <h1>Créer une réunion</h1>
                            </div>
                        </div>
                        <div class="card-body">
                            <form id="form1" class="form-validate">
                                <div class="form-group p-b-5">
                                    <label for="title">Titre</label>
                                    <input type="text" class="form-control" name="title" placeholder="Entrez le titre" required>
                                </div>
                                <div class="form-group p-b-5">
                                    <label for="start">Début</label>
                                    <input class="form-control" name="start" type="datetime-local" value="2011-08-19T13:45:00">
                                </div>
                                <div class="form-group p-b-5">
                                    <label for="end">Fin</label>
                                    <input class="form-control" name="end" type="datetime-local" value="2011-08-19T13:45:00">
                                </div>
                                <div class="form-group p-b-5">
                                    <label for="description">Description</label>
                                    <input type="text" class="form-control" name="description" placeholder="Entrez la description de l'activité">
                                </div>
                                <div class="text-right form-group">
                                    <a href="calender.php" class="btn btn-danger" style="color:white">Annuler</a>
                                    <input class="btn btn-success" type="submit" name="button" id="button" value="Valider" />
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>

    </div>

    <?php include("include-footer.php");?>
    <?php include("include-script.php");?>
    
    </body>
</html>