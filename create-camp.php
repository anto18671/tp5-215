<?php include("test-log.php");?>

<!DOCTYPE html>
<html lang="fr">
<html>
    <?php include("include-head.php");?>
    <body>
    <div class="body-inner">

    <?php include("include-header-profil.php");?>

        <section id="page-content" data-bg-parallax="images/29.jpg">
            <div class="container">
                <div class="row">
                <div class="content col-lg-9 center">
                    <div class="card">
                        <div class="card-header">
                            <div class="content col-lg-9 center" style="text-align: center;">
                                <h2>Créer un nouveau camp</h2>
                            </div>
                        </div>
                        <div class="card-body">
                            <form id="form1" class="form-validate">
                                    <div class="form-group p-b-5">
                                        <label for="campName">Nom du camp</label>
                                        <input type="text" class="form-control" name="campName" placeholder="Entrez le titre" required>
                                    </div>
                                    <div class="form-group p-b-5">
                                        <label for="location">Adresse</label>
                                        <input type="text" class="form-control" name="location" placeholder="Entrez l'heure de début" required>
                                    </div>
                                    <div class="form-group p-b-5">
                                        <label for="campTelephone">Téléphone</label>
                                        <input type="tel" class="form-control" name="campTelephone" placeholder="Entrez votre numéro de téléphone à 9 chiffres : 0000000000" pattern="[0-9]{9}" required>
                                    </div>
                                    <div class="text-right form-group">
                                        <a href="calender.php" class="btn btn-danger" style="color:white">Annuler</a>
                                        <input class="btn btn-success" type="submit" name="button" id="button" value="Valider" />
                                    </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </section>

    </div>

    <?php include("include-footer.php");?>
    <?php include("include-script.php");?>
    
    </body>
</html>