<?php include("test-log.php");?>

<!DOCTYPE html>

<body>
    <?php include("include-head.php");?>
    
    <div class="body-inner">
        <?php include("include-header-profil.php");?>
        
        <section id="page-content" data-bg-parallax="images/29.jpg" >
            <div class="container">

                <div class="card bg-light">

                    <div class="card-header bg-light">
                        <div class="row">
                            <div class="col-lg-3" >
                                <h2>Calendrier</h2>
                            </div>
                            <div class="col-lg-9" >
                                <ul class="nav nav-tabs nav-fill">
                                    <li class="nav-item"><a class="nav-link" href="profil.php">Profil</a></li>
                                    <li class="nav-item"><a class="nav-link active" href="calender.php">Calendrier</a></li>
                                    <li class="nav-item"><a class="nav-link" href="document.php">Document</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="row m-b-20">
                            <div class="col-lg-3" ></div>
                            <div class="col-lg-9"> 
                                <a href="create-event.php" class="btn btn-warning float-right m-r-5" style="color:black">Créer une activité</a>
                                <a href="create-meeting.php" class="btn btn-warning float-right m-r-5" style="color:black">Créer une réunion</a>
                                <a href="create-camp.php" class="btn btn-warning float-right m-r-5" style="color:black">Créer un camp</a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">

                        <div class="row">
                            
                            <div class="row">
                                <div class="col-lg-11 center">
                                    <div id="calendar"></div>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="row m-b-20">
                            <div class="content col-lg-3" ></div>
                            <div class="col-lg-9"> 
                                <a href="create-event.php" class="btn btn-warning float-right m-r-5" style="color:black">Créer une activité</a>
                                <a href="create-meeting.php" class="btn btn-warning float-right m-r-5" style="color:black">Créer une réunion</a>
                                <a href="create-camp.php" class="btn btn-warning float-right m-r-5" style="color:black">Créer un camp</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php include("include-footer.php");?>
    <?php include("include-script.php");?>

<a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>

<script src="js/jquery.js"></script>
<script src="js/plugins.js"></script>

<script src="js/functions.js"></script>
<script src='plugins/moment/moment.min.js'></script>
<script src='plugins/fullcalendar/fullcalendar.min.js'></script>
<script>
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            defaultDate: '2019-01-12',
            navLinks: true, // can click day/week names to navigate views
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: [{
                    title: 'All Day Event',
                    start: '2019-01-01',
                },
                {
                    title: 'Long Event',
                    start: '2019-01-07',
                    end: '2019-01-10'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: '2019-01-09T16:00:00'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: '2019-01-16T16:00:00'
                },
                {
                    title: 'Conference',
                    start: '2019-01-11',
                    end: '2019-01-13',
                    description: "Lorem ipsum dolor sit incid idunt ut",
                },
                {
                    title: 'Meeting',
                    start: '2019-01-12T10:30:00',
                    end: '2019-01-12T12:30:00'
                },
                {
                    title: 'Lunch',
                    start: '2019-01-12T12:00:00'
                },
                {
                    title: 'Meeting',
                    start: '2019-01-12T14:30:00'
                },
                {
                    title: 'Happy Hour',
                    start: '2019-01-12T17:30:00'
                },
                {
                    title: 'Dinner',
                    start: '2019-01-12T20:00:00'
                },
                {
                    title: 'Birthday Party',
                    start: '2019-01-13T07:00:00',
                }
            ]
        });
    </script>
</body>
</html>